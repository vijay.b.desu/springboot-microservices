package com.springbootpoc.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import com.springbootpoc.demo.model.Greeting;
import com.springbootpoc.demo.model.User;
import java.util.function.Function;
/**
 * 
 * @author vijay
 *
 * https://docs.microsoft.com/en-us/azure/developer/java/spring-framework/getting-started-with-spring-cloud-function-in-azure
 */
@SpringBootApplication
public class SpringBootPocAzureFunctionsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootPocAzureFunctionsApplication.class, args);
	}
	
	@Bean
    public Function<User, Greeting> hello() {
        return user -> new Greeting("Welcome, " + user.getName());
    }

}
